#===============================================================================
#USAGE : ./ms1-menu.sh orientation
#DESCRIPTION : Opens a terminal menu to execute restart, shutdown, turns-creen, keyboard-language scripts.
#PARAMS: 
#TIPS:
#===============================================================================
#
#!/bin/bash
#START
#Provide a menu of options for the user. 

PS3='Please select a command:'
commands=("restart" "shutdown" "turn-screen" "keyboard-language")
select com in "${commands[@]}"

do 
    case $com in 
        "restart")
        sh ./restart.sh
        ;;
        "shutdown")
        sh ./shutdown.sh
        ;;
        "turn-screen")
        read -p "What direction do you want to turn your screen?" 
        sh ./turn-screen.sh $REPLY
        ;;
        "keyboard-language")
        read -p "What is the language code that you want?" 
        sh ./keyboard-language.sh $REPLY
        ;;
        *) 
        echo "Error: Invalid command."
    esac
done

#Prompt for inputs if necessary 
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
