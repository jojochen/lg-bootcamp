#===============================================================================
#USAGE : ./turnscreen.sh orientation
#DESCRIPTION : Pass as an argument de degrees of the rotation of the screen.
#PARAMS: orientation can be right, left, inverted
#TIPS:
#===============================================================================
#
#!/bin/bash
#START

if [ $1 = "right" ]; then 
    echo "Rotating screen right."
    xrandr -o right
elif [ $1 = "left" ]; then
    echo "Rotating screen left."
    xrandr -o left
elif [ $1 = "inverted" ]; then
    echo "Inverting screen."
    xrandr -o inverted
else 
    echo "Error: incorrect arguments."
fi 
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
